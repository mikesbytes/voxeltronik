-- example terrain generator
local tg = {}

tg.noise = Noise.new(os.time())
tg.ygrad = YGradient.new(0, 128)
tg.yturb = YTurbulence.new(tg.ygrad, tg.noise, 16)
tg.bdec = BinaryDecorator.new(tg.yturb, 0, 1, 0)
tg.final_decorator = FrostingDecorator.new(tg.bdec, 0, 2, 3)

return tg
