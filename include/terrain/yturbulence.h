/*
 * Y Axis turbulence
 */

#pragma once
#include <memory>

#include "terrain/noisemodule.h"

namespace sol {
class state;
}

namespace vtk { namespace noise {

class YTurbulence : public NoiseModule {
public:
	YTurbulence(NoiseModule& input, NoiseModule& modifier, const double& multiplier);
	double get3D(const double&x, const double&y, const double&z);

	static void registerScriptInterface(::sol::state& lua);
protected:
	NoiseModule& mInput;
	NoiseModule& mModifier;
	double mMultiplier;
};
  }} // vtk::noise
