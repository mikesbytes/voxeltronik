#pragma once

#include <glm/glm.hpp>

namespace sol {
class state;
}

namespace vtk { namespace noise {

/// Base class for creating decorators
/**
 * Decorators take a position and return a voxel id
 * see BinaryDecorator for a very simple example. Most
 * frequently, they will take output from NoiseModules
 * or other Decorators.
 */
class Decorator {
public:
	virtual unsigned get3D(const glm::ivec3& pos) { return 0; }
	static void registerScriptInterface(::sol::state& lua);
protected:
};

}}
