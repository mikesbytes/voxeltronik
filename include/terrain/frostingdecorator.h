#pragma once

#include "terrain/decorator.h"

namespace sol {
class state;
}

namespace vtk { namespace noise {

/// Covers voxels exposed to air
/**
 * The FrostingDecorator takes a decorator input and checks whether a voxel is exposed to air.
 * if it is, it is replaced by voxels of a given type down to the specified depth. A depth of
 * 1 will replace only the top voxel for example. You can also set an offset, a positive offset
 * will start frosting up into the air, and a negative offset will start frosting under the
 * exposed voxel.
 * 
 * An example usage (and the original purpose) would be to place dirt and sod on top of stone
 * to cover all vertically exposed voxels in grass.
 */
class FrostingDecorator : public Decorator {
public:
	FrostingDecorator(Decorator& input, const int& yOffset, const int& depth, const unsigned& type);
	unsigned get3D(const glm::ivec3& pos);

	static void registerScriptInterface(::sol::state& lua);
protected:
	Decorator& mInput;
	int mYOffset;
	int mDepth;
	unsigned mType;
};
}}
