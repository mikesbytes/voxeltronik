#pragma once

#include <tuple>
#include <glm/glm.hpp>
#include <array>

namespace vtk {

enum class Direction : unsigned int {
    UP = 0,
    DOWN = 1,
    NORTH = 2,
    SOUTH = 3,
    WEST = 4,
    EAST = 5,
};


glm::ivec3 worldPosToChunkPos(const glm::ivec3& pos);
glm::ivec2 worldPosToChunkPos(const glm::ivec2& pos);

//takes chunk and local positions, returns a world position
glm::ivec3 chunkPosToWorldPos(const glm::ivec3& cPos, const glm::ivec3& lPos);

//takes a chunk/local pos and returns a chunk,local pos pair (works for out of bounds coords)
std::pair<glm::ivec3, glm::ivec3> worldPosToLocalPos(const glm::ivec3& pos);
std::pair<glm::ivec3, glm::ivec3> localPosToLocalPos(const glm::ivec3& cPos, const glm::ivec3& lPos);

/// Convert Direction to a glm::ivec3 unit vector
constexpr glm::ivec3 DirectionToVec(const Direction& direction);

/// basic hash function for glm::ivec3
struct ivec3Hash : public std::unary_function<glm::ivec3, std::size_t> {
	std::size_t operator()(const glm::ivec3& k) const {
		std::size_t seed = 3;
		for(int i = 0; i < 3; ++i) {
			seed ^= k[i] + 0x9e3779b9 + (seed << 6) + (seed >> 2);
		}
		return seed;
	}
};

/// hash function for glm::ivec2
struct ivec2Hash : public std::unary_function<glm::ivec2, std::size_t> {
	std::size_t operator()(const glm::ivec2& k) const {
		std::size_t seed = 3;
		for(int i = 0; i < 2; ++i) {
			seed ^= k[i] + 0x9e3779b9 + (seed << 6) + (seed >> 2);
		}
		return seed;
	}
};

/// Iterate over all voxels +1 in any Direction from given position
/** Accepts a function f that takes glm::ivec3
 *
 * example, given a function `void f(glm::ivec3) {...}`,calling
 * `DirectionCoordinateIteration(f, glm::ivec3(1,2,3))` will result
 * in `f` being called 6 times with the following args
 * 
 * `f()` is called with the following args:  
 * - glm::ivec3(1,3,3)
 * - glm::ivec3(1,1,3)
 * - glm::ivec3(2,2,3)
 * - glm::ivec3(0,2,3)
 * - glm::ivec3(1,2,4)
 * - glm::ivec3(1,2,2)
 */
template <typename Functor>
void DirectionCoordinateIteration(Functor&& func, const glm::ivec3& pos = glm::ivec3(0,0,0)) {
	std::forward<Functor>(func)(glm::ivec3(pos.x    , pos.y + 1, pos.z    ));
	std::forward<Functor>(func)(glm::ivec3(pos.x    , pos.y - 1, pos.z    ));
	std::forward<Functor>(func)(glm::ivec3(pos.x + 1, pos.y    , pos.z    ));
	std::forward<Functor>(func)(glm::ivec3(pos.x - 1, pos.y    , pos.z    ));
	std::forward<Functor>(func)(glm::ivec3(pos.x    , pos.y    , pos.z + 1));
	std::forward<Functor>(func)(glm::ivec3(pos.x    , pos.y    , pos.z - 1));
}

}
