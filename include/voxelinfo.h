#pragma once

#include "voxelutils.h"
#include "sol.hpp"

#include <map>
#include <vector>
#include <array>
#include <glm/glm.hpp>

namespace sol {
class state;
}

namespace vtk {

struct VoxelType{
	std::string tag;
	std::string name;
	bool transparent;
	unsigned short emission;

	std::array<unsigned, 6> faceTextures;
	std::array<unsigned, 6> faceOrientations;
};

class World;

class VoxelInfo {
public:
	VoxelInfo();
	bool tagExists(const std::string& tag);
	unsigned idFromTag(const std::string& tag);
	bool newType(sol::table table);

	VoxelType& getTypeByID(const unsigned& id);
	VoxelType& getTypeByTag(const std::string& tag);	
	
	unsigned getTextureIndex(const unsigned& id, const Direction& face);

    void setTransparent(const unsigned& id, const bool& transparent);
	bool isTransparent(const unsigned& id);

	unsigned short getEmission(const unsigned& id);

    World* linkedWorld;

	static void registerScriptInterface(::sol::state &lua);
protected:
	unsigned mHighestID;
	std::map<unsigned, VoxelType> mVoxelTypes;
	std::map<std::string, unsigned> mVoxelTags; //tag map for quick id lookups
};

}
