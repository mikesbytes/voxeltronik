#VTK Coordinate system

##The basics

In "world space" (more on this further down), voxels are indexed with an 3 dimensional integer vector
of the form (x,y,z)
internally represented by glm::ivec3. So a voxel at 3,5,20 is that position relative to 0,0,0 in the
world.

Associated with these coordinates is a direction system. These directions are as follows:

- UP: y+

- DOWN: y-

- NORTH: z+

- WEST: x+

- SOUTH: z-

- EAST: x-


##Local, World, and Chunk Positions

A world position is described above. The further you go from 0,0,0 the larger
the values become.

In VTK, the world is divided into chunks, and each chunk has a position. These positions index the
individual chunks much like the world space indexes voxels. For example, the chunk at 0,0,0 contains
all the voxels in a cube from 0,0,0 to 15,15,15. The chunk at 1,2,3 on the other hand would contain
all the voxels from 16,32,48 to 31,47,63.

When you are working inside a chunk, you will frequently see "local" positions used. These
positions are relative to the current chunk, going from 0 to 15 on all axis. Unlike the world space,
the local positions inside every chunk are indexed from 0 to 15, not just the one at the origin.
