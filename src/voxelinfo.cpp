#include "voxelinfo.h"
#include "world.h"

#include "sol.hpp"

namespace vtk {

VoxelInfo::VoxelInfo() :
	mHighestID(0)
{
	VoxelType air;
	air.tag = "air";
	air.name = "Air";
	air.transparent = true;
	air.emission = 0;
	mVoxelTypes[0] = air;
	mVoxelTags["air"] = 0; 
}

bool VoxelInfo::tagExists(const std::string &tag) {
	return mVoxelTags.find(tag) != mVoxelTags.end();
}

unsigned VoxelInfo::idFromTag(const std::string& tag) {
	auto s = mVoxelTags.find(tag);
	if (s == mVoxelTags.end()) return 0;
	return s->second;
}

bool VoxelInfo::newType(sol::table table) {
	unsigned newID = ++mHighestID;
	VoxelType newType;
	newType.tag = table.get_or<std::string>("tag", "");
	if (newType.tag == "") return false;

	newType.name = table.get_or<std::string>("name", "");
	//newType.transparent = table.get_or<bool>("transparent", (bool)false);
	auto trans = table.get<sol::optional<bool>>("transparent");
	if(trans) {
		newType.transparent = trans.value();
	} else {
		newType.transparent = false;
	}
	
	newType.emission = table.get_or<unsigned short>("emission", 0x0);

	sol::table texTab = table["textures"];
	auto allTex = texTab.get<sol::optional<unsigned>>("all");
	if (allTex) {
		for (int i = 0; i < 6; ++i) {
			newType.faceTextures[i] = allTex.value();
		}
	}
	auto x = texTab.get<sol::optional<unsigned>,
	                    sol::optional<unsigned>,
	                    sol::optional<unsigned>,
	                    sol::optional<unsigned>,
	                    sol::optional<unsigned>,
	                    sol::optional<unsigned>>
		("top", "bottom", "north", "south", "east", "west");
	if (std::get<0>(x))	newType.faceTextures[0] = std::get<0>(x).value();
	if (std::get<1>(x))	newType.faceTextures[1] = std::get<1>(x).value();
	if (std::get<2>(x))	newType.faceTextures[2] = std::get<2>(x).value();
	if (std::get<3>(x))	newType.faceTextures[3] = std::get<3>(x).value();
	if (std::get<4>(x))	newType.faceTextures[4] = std::get<4>(x).value();
	if (std::get<5>(x))	newType.faceTextures[5] = std::get<5>(x).value();

	//set the type if all is well
	mVoxelTypes[newID] = newType;
	mVoxelTags[newType.tag] = newID;
	return true;
}

VoxelType& VoxelInfo::getTypeByID(const unsigned& id) {
	return mVoxelTypes[id];
}

VoxelType& VoxelInfo::getTypeByTag(const std::string &tag) {
	return mVoxelTypes[mVoxelTags[tag]];
}

unsigned VoxelInfo::getTextureIndex(const unsigned int &id, const Direction &face) {
	return mVoxelTypes[id].faceTextures[static_cast<unsigned>(face)];
}


void VoxelInfo::setTransparent(const unsigned& id, const bool& transparent) {
    mVoxelTypes[id].transparent = transparent;
}

bool VoxelInfo::isTransparent(const unsigned& id) {
    return mVoxelTypes[id].transparent;
}


unsigned short VoxelInfo::getEmission(const unsigned int &id) {
	return mVoxelTypes[id].emission;
}

void VoxelInfo::registerScriptInterface(::sol::state &lua) {
	lua.new_usertype<VoxelInfo>("VoxelInfo",
	                            "new_type", &VoxelInfo::newType,
	                            "get_type_by_id", &VoxelInfo::getTypeByID,
	                            "get_type_by_tag", &VoxelInfo::getTypeByTag);

	lua.new_usertype<VoxelType>("VoxelType",
	                            "tag", &VoxelType::tag,
	                            "name", &VoxelType::name);
}
}

