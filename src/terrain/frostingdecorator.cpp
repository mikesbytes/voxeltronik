#include "terrain/frostingdecorator.h"

#include "sol.hpp"

namespace vtk { namespace noise {

FrostingDecorator::FrostingDecorator(Decorator& input, const int& yOffset, const int& depth, const unsigned& type) :
	mInput(input),
	mYOffset(yOffset),
	mDepth(depth),
	mType(type)
{

}

unsigned FrostingDecorator::get3D(const glm::ivec3& pos) {
	auto inType = mInput.get3D(pos);
	if (inType == 0) return inType; //return if air
	auto checkPos = glm::ivec3(pos.x, pos.y + mDepth, pos.z);
	if (mInput.get3D(checkPos) == 0) return mType;
	return inType;
}

void FrostingDecorator::registerScriptInterface(::sol::state& lua) {
	lua.new_usertype<FrostingDecorator>("FrostingDecorator",
	                                    sol::constructors<FrostingDecorator(
		 Decorator&, const int&, const int&, const unsigned&)>(),
	                                    sol::base_classes, sol::bases<Decorator>());
}

}}
