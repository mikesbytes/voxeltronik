#include <iostream>

#include "terrain/yturbulence.h"
#include "sol.hpp"

namespace vtk { namespace noise {

YTurbulence::YTurbulence(NoiseModule& input, NoiseModule& modifier, const double& multiplier) :
	mInput(input),
	mModifier(modifier),
	mMultiplier(multiplier)
{

}

double YTurbulence::get3D(const double&x, const double&y, const double&z) {
	double mod = mModifier.get3D(x,y,z) * mMultiplier;
	return mInput.get3D(x, y + mod, z);
}

void YTurbulence::registerScriptInterface(::sol::state &lua) {
	lua.new_usertype<YTurbulence>("YTurbulence",
	                              sol::constructors<YTurbulence(NoiseModule&, NoiseModule&, const double&)>(),
	                              sol::base_classes, sol::bases<NoiseModule>());
}

  }}
